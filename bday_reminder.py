#!/usr/bin/env python3

import re
import argparse
import csv
import traceback
import sqlite3 as sl
import subprocess  # nosec b404
from os.path import abspath, dirname, exists
from sys import exit, argv
from time import sleep
from datetime import date

curdir = f"{dirname(abspath(__file__))}"
dbfile = f"{curdir}/birthdays.db"

con = None
crsr = None

MAIL_EXE = "/usr/bin/mail"


def init():
    global con
    global crsr
    if not exists(dbfile):
        print("No birthday database detected. Do you want to create a new one? [y/N]: ")
        ans = input()
        if ans.lower() != "y":
            exit(0)
        con = sl.connect(dbfile)
        with con:
            con.execute(
                """
                CREATE TABLE bdays (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                name TEXT NOT NULL,
                date TEXT NOT NULL,
                notified INTEGER NOT NULL DEFAULT 0
                );
            """
            )
        con.commit()
    else:
        con = sl.connect(dbfile)

    crsr = con.cursor()


def list_prime():
    global crsr
    data = crsr.execute("SELECT * FROM bdays")
    return data


def do_list(args):
    data = list_prime()
    for row in data:
        print(row)


def do_check(args):
    global crsr

    sql = f"select * from bdays where notified = 0 and date >= date('now') and date <= date('now', '+{args.within} days')"
    idns = []
    try:
        data = crsr.execute(sql)

        for idn, name, dob, notified in data:
            dob = dob.split(" ")[0]
            sp = subprocess.Popen(
                [
                    MAIL_EXE,
                    "-A",
                    "gmail",
                    "-s",
                    f"Birthday - {dob}",
                    "ptchinster@gmail.com",
                ],
                stdin=subprocess.PIPE,
                shell=False,  # nosec b603
            )
            sp.communicate(input=name.encode())
            idns.append(idn)

        for idn in idns:
            sql = "update bdays set notified=TRUE where id=?"
            crsr.execute(sql, (idn,))
        con.commit()

    except Exception as e:  # TODO catch the real exception type
        print(f"Exception: {e}")
        traceback.print_exc()
        exit(-1)

    sql = "select * from bdays where notified = 1 and date < date('now')"
    idns = []
    try:
        data = crsr.execute(sql)
        for idn, name, dob, notified in data:
            idns.append(idn)

        for idn in idns:
            sql = "update bdays set notified=FALSE where id=?"
            crsr.execute(sql, (idn,))
        con.commit()
    except Exception as e:  # TODO
        print(f"Error!: {e}")
        traceback.print_exc()
        exit(-1)

    sql = "select * from bdays where date < date('now')"
    idns = []
    try:
        data = crsr.execute(sql)
        for idn, name, dob, notified in data:
            idns.append((idn, dob))

        for idn, date in idns:
            sql = "update bdays set date=date(?, '+1 year') where id=?"
            crsr.execute(sql, (date, idn))
        con.commit()
    except Exception as e:  # TODO
        print(f"Error!: {e}")
        traceback.print_exc()
        exit(-1)


def do_add(args):
    add_prime(args.name, args.date)


def do_delete(args):
    delete_prime(args.rowid)


def add_prime(name, date):
    global con
    global crsr
    regex = re.search(r"^'\d{4}-\d{2}-\d{2}'$", date)  # TODO accept YYYYMMDD as well
    if regex is None:
        print(f'The string "{date}" is not in the format "YYYY-MM-DD" (Name: {name})')
        return
    # TODO check to make sure dates are valid?

    sql = f"INSERT INTO bdays (name, date) values({name}, datetime({date}))"
    try:
        crsr.execute(sql)
        con.commit()
    except Exception as e:
        print(f"Error!: {e}")
        traceback.print_exc()
        exit(-1)

    print(f"Added {name} with a bday of {date}")


def delete_prime(rowid):
    global con
    global crsr
    regex = re.search(r"^'\d{1,}'$", rowid)
    if regex is None:
        print("You must enter a rowid as a number")
        return
    sql = f"DELETE FROM bdays WHERE id={rowid}"
    try:
        crsr.execute(sql)
        con.commit()
    except Exception as e:
        print(f"Error!: {e}")
        traceback.print_exc()
        exit(-1)
    print(f"Deleted row {rowid}")


def do_export(args):
    today = str(date.today()).replace("-", "")
    outfile = f"{curdir}/bdays.BAK.{today}"
    data = list_prime()
    with open(outfile, mode="w") as file:
        csvFile = csv.writer(file)
        for idn, name, dob, notified in data:
            d = list((name, dob.split(" ")[0]))
            csvFile.writerow(d)
    print(f"Wrote to {outfile}")


def do_import(args):
    importFile = args.file.strip("'")
    with open(f"{curdir}/{importFile}", mode="r") as file:
        csvFile = csv.reader(file)
        for name, date in csvFile:
            add_prime(
                f"'{name}'", f"'{date}'"
            )  # because when this comes through as a command line param, it will have ' on either side of it


if __name__ == "__main__":
    if len(argv) < 2:
        print('Try running with at least 1 argument, like "help"')
        exit(0)

    all_args = argparse.ArgumentParser(description="Birthday Tracker Helper")
    subparsers = all_args.add_subparsers(
        title="Commands", description="valid subcommands"
    )

    parser_add = subparsers.add_parser("add")
    parser_add.add_argument(
        "--name", required=True, action="store", type=ascii, help="Name of person"
    )
    parser_add.add_argument(
        "--date", required=True, action="store", type=ascii, help="DoB of person"
    )
    parser_add.set_defaults(func=do_add)

    parser_add = subparsers.add_parser("delete")
    parser_add.add_argument(
        "--rowid",
        required=True,
        action="store",
        type=ascii,
        help="ID of entry to delete",
    )
    parser_add.set_defaults(func=do_delete)

    parser_list = subparsers.add_parser("list")
    parser_list.set_defaults(func=do_list)

    parser_check = subparsers.add_parser("check")
    parser_check.add_argument(
        "--within",
        required=True,
        action="store",
        type=int,
        help="Within this many days",
    )
    parser_check.set_defaults(func=do_check)

    parser_check = subparsers.add_parser("import")
    parser_check.add_argument(
        "--file",
        required=True,
        action="store",
        type=ascii,
        help="Import from a CSV (name,dob) file",
    )
    parser_check.set_defaults(func=do_import)

    parser_check = subparsers.add_parser("export")
    parser_check.set_defaults(func=do_export)

    args = all_args.parse_args()
    init()
    args.func(args)
